/**
 * Copyright (C) 2016, Antony Holmes
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of copyright holder nor the names of its contributors 
 *     may be used to endorse or promote products derived from this software 
 *     without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.abh.common.bioinformatics.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.abh.common.bioinformatics.genomic.Chromosome;
import org.abh.common.io.Io;
import org.abh.common.text.TextUtils;





// TODO: Auto-generated Javadoc
/**
 * The class FeaturesNameSearch.
 */
public class FeaturesNameSearch implements Iterable<Feature> {
	
	/**
	 * The features.
	 */
	protected Map<String, Feature> features = new HashMap<String, Feature>();

	/**
	 * The type.
	 */
	protected String type;

	/**
	 * Instantiates a new features name search.
	 *
	 * @param type the type
	 */
	public FeaturesNameSearch(String type) {
		this.type = type;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public final String getType() {
		return type;
	}

	/**
	 * Cache features.
	 *
	 * @param file the file
	 */
	public void cacheFeatures(File file) {
		features.clear();

		short chromosomeCount = 0;
		int featureCount = 0;

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));

			String line;

			try {
				// skip header
				line = reader.readLine();

				while ((line = reader.readLine()) != null) {
					if (Io.isEmptyLine(line)) {
						continue;
					}

					List<String> row = TextUtils.fastSplitRemoveQuotes(line);

					Feature feature =
							new Feature(row.get(0), Chromosome.parse(row.get(1)), Integer.parseInt(row.get(2)), Integer.parseInt(row.get(3)));

					features.put(feature.getName(), feature);

					++featureCount;
				}
			} finally {
				System.out.println("Loaded " + chromosomeCount + " groups with " + featureCount + " features from " + file + ".");

				reader.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the feature.
	 *
	 * @param name the name
	 * @return the feature
	 */
	public final Feature getFeature(String name) {
		if (features.containsKey(name)) {
			return features.get(name);
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator<Feature> iterator() {
		return features.values().iterator();
	}

	/**
	 * Size.
	 *
	 * @return the int
	 */
	public int size() {
		return features.size();
	}
}
